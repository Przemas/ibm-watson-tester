﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBMwatson
{
    public class ResultBE
    {
        public Result[] results { get; set; }

        public int result_index { get; set; }
    }

    public class Result
    {
        public Alternative[] alternatives { get; set; }

        public bool final { get; set; }
    }

    public class Alternative
    {
        public double confidence { get; set; }

        public string transcript { get; set; }
    }
}