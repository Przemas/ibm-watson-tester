﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IBM.WatsonDeveloperCloud;
using IBM.WatsonDeveloperCloud.SpeechToText.v1;
using IBM.WatsonDeveloperCloud.Util;
using Jil;
using Microsoft.Win32;

namespace IBMwatson
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string url = "https://stream-fra.watsonplatform.net/speech-to-text/api/v1/recognize";

        private static readonly HttpClient client = new HttpClient();
        public MainWindow()
        {
            InitializeComponent();
            url += "?model=de-DE_BroadbandModel";
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            string file = File_TextBlock.Text;
            byte[] audioBytes = File.ReadAllBytes(file);

            string extension = file.Split('.')[1];
            HttpContent content = new ByteArrayContent(audioBytes);
            content.Headers.Add("Content-Type", String.Format("audio/{0}", extension));

            var response = await client.PostAsync(url, content);
            string responseString = await response.Content.ReadAsStringAsync();
            var finalResultString = "";
            double confidence = 0;

            using (var input = new StringReader(responseString))
            {
                var result = JSON.Deserialize<ResultBE>(input);
                finalResultString = result.results?[0]?.alternatives?[0]?.transcript;
                confidence = (double)result.results?[0]?.alternatives?[0]?.confidence;
            }

            watch.Stop();

            Time_tb.Text = watch.Elapsed.ToString();
            confidence_tb.Text = confidence.ToString();
            result_tb.Text = finalResultString;
        }

        private void File_button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if ((bool)ofd.ShowDialog())
            {
                File_TextBlock.Text = ofd.FileName;
            }
        }

        private void acceptApi_button_Click(object sender, RoutedEventArgs e)
        {
            string apikey = api_tb.Text;
            string auth = "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("apikey:{0}", apikey)));
            client.DefaultRequestHeaders.Add("Authorization", auth);
            api_tb.IsEnabled = false;
            api_tb.IsReadOnly = true;
            acceptApi_button.IsEnabled = false;
            File_button.IsEnabled = true;
            translate_button.IsEnabled = true;
        }
    }
}